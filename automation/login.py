#!/usr/bin/env python
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert

class login:
    def __init__(self, driver):
        self.driver=driver
    def login(self,username,password):
        self.driver.get("https://id.atlassian.com/login?continue=https%3A%2F%2Fjira.atlassian.com%2Fsecure%2FDashboard.jspa&application=jac")
        # goto https://jira.atlassian.com/secure/Dashboard.jspa
        # click login
        self.driver.find_element_by_xpath(".//*[@id='username']").send_keys(username)
        self.driver.find_element_by_xpath(".//*[@id='password']").send_keys(password)
        self.driver.find_element_by_xpath(".//*[@id='form-login']/div[3]").click()
        return self
    def create_issue(self):
        self.driver.find_element_by_xpath(".//*[@id='create_link']").click()
        return self.driver.title

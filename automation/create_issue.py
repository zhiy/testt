#!/usr/bin/env python
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert

class create_issue:
    def __init__(self, driver):
        self.driver=driver
    def create(self,summary, desc):
        self.driver.find_element_by_xpath(".//*[@id='summary']").send_keys(summary)
        self.driver.find_element_by_xpath(".//*[@id='description']").send_keys(desc)
        self.driver.find_element_by_xpath("").click(".//*[@id='create-issue-submit']")
        # goto page report by me
        self.driver.get("https://jira.atlassian.com/browse/TST-65543?filter=-2")
        return self.driver.page_source


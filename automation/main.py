import unittest
import sys
import time
from login import *
from create_issue import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
import os

class bug_test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()

    def tearDown(self):
        self.driver.close()

    def test_create_issue(self):
        # first login
        login_page=login(self.driver)
        login_page.login("your email","your pass")
        # click create issue
        login_page.create_issue()
        # input issue
        issue_page=create_isue(self.driver)
        text=issue_page.create("test_issue1","test")
        self.assertTrue(text.find("test_issue1")>-1)

if __name__=="__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(bug_test)
    unittest.TextTestRunner(verbosity=2).run(suite)

1. As an admin or sys. admin, can he/she edit any user's profile?
2. can readable users see the edit profile menu?
3. on change password sceen, should let user input their old password
4. on edit details screen, user can change username and email, which is the uniq id for the user? can it be changed?
5. on edit profile screen, what information can be edited? profile photo?
6. on edit details screen, is there validation for email and username?
7. on change password screen, is there validation for password length and format?
